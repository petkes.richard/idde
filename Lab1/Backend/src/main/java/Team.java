/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author petkes
 */
public class Team {
	
    private String name;
    private String country;
    private String league;
    private int titles;
		
    Team(String name, String country, String league, int titles){
        this.name = name;
        this.country = country;
        this.league = league;
        this.titles = titles;
    }
    
    Team() {
        this.name = null;
        this.country = null;
        this.league = null;
        this.titles = 0;
    }
    
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
       this.name = name;
    }
    
    public String getCountry() {
        return this.name;
    }

    public void setCountry(String country) {
       this.country = country;
    }
    
    public String getLeague() {
        return this.league;
    }

    public void setLeague(String league) {
       this.league = league;
    }
    
    public int getTitles() {
        return this.titles;
    }

    public void setTitles(int titles) {
       this.titles = titles;
    }
	
}
