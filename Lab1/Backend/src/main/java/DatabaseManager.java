
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/*
 * @author petkes
 */

public class DatabaseManager {
    
    private ResultSet resultSet;
    private List<Team> teams;
    private ConnectionManager con;
        


    public List<Team> getTeams(){
        String sql;
	sql = "SELECT Name, Country, League, Titles FROM team";
        con =  new ConnectionManager();
        resultSet = con.executeCommand(sql);
        System.out.println(resultSet);
        Team result;
        this.teams =  new ArrayList<Team>();
        try {
            while(resultSet.next()){
                result = new Team();
                result.setName(resultSet.getString(1));
                result.setCountry(resultSet.getString(2));
                result.setLeague(resultSet.getString(3));
                result.setTitles(resultSet.getInt(4));
                this.teams.add(result);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teams;
    }
}
