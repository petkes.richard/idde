import com.mysql.jdbc.ResultSetImpl;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author petkes
 */
public class ConnectionManager{
     
    private Statement statement;
    private Connection connection;
    private ResultSetImpl resultSet;
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
    static final String DB_URL = "jdbc:mysql://localhost/football_team";
    static final String USER = "root";
    static final String PASS = "ddc9e55b";      

    public ConnectionManager(){
        try {
            this.connection = (Connection) DriverManager.getConnection(DB_URL, USER, PASS);
            this.statement = (Statement) connection.createStatement();
            System.err.println("Connection established with database...");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
     
    public ResultSetImpl executeCommand(String command){
        try {
            this.resultSet = (ResultSetImpl) statement.executeQuery(command);
        } catch (SQLException e) {
            e.printStackTrace();
        }
         return resultSet;
    }
	   
}
