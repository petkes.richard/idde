import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;


public class App extends JFrame {
    
    private JButton button;
    private JTable table;
    private JScrollPane scroll;
    private DefaultTableModel model;
    private Boolean visited = true;
    private ConnectionManager con;
    private DatabaseManager db;
    
    public App() {
        this.table = new JTable();
        this.scroll = new JScrollPane(this.table);
        this.model = (DefaultTableModel) table.getModel();
        this.setTitle("Fifa teams");
        this.setSize(900, 700);
        this.setLocation(350,300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.button = new JButton("Show database");
        this.add(this.button, BorderLayout.NORTH);
        this.add(this.scroll, BorderLayout.CENTER);
        
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                while ( visited ){
                    visited = false;
                    db = new DatabaseManager();
                    List<Team> list = db.getTeams();
                    model.addColumn("Team");
                    model.addColumn("Country");
                    model.addColumn("League");
                    model.addColumn("Titles");
                    
                    Team team;
                    for (int i = 0; i < list.size(); i++) {
                        team = list.get(i);
                        model.addRow(new Object[]{team.getName(), team.getCountry(), team.getLeague(), team.getTitles()});
                    }
                }
            }
        });
        
    }
}